function changePrice() {
  let basic = document.getElementById("basic");
  let professional = document.getElementById("professional");
  let master = document.getElementById("master");

  if (basic.innerHTML.trim() == "199.99") {
    basic.innerHTML = "19.99";
  } else {
    basic.innerHTML = "199.99";
  }

  if (professional.innerHTML.trim() == "249.99") {
    professional.innerHTML = "24.99";
  } else {
    professional.innerHTML = "249.99";
  }

  if (master.innerHTML.trim() == "399.99") {
    master.innerHTML = "39.99";
  } else {
    master.innerHTML = "399.99";
  }
}
